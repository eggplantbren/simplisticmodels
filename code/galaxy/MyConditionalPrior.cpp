#include "MyConditionalPrior.h"
#include "DNest4/code/DNest4.h"
#include "Data.h"
#include <cmath>

using namespace DNest4;

MyConditionalPrior::MyConditionalPrior()
:image_length(
                sqrt((Data::get_instance().get_x_max() - Data::get_instance().get_x_min())*
                     (Data::get_instance().get_y_max() - Data::get_instance().get_y_min()))
             )
{
}

void MyConditionalPrior::from_prior(RNG& rng)
{
    mu_mass = exp(tan(M_PI*(0.01 + 0.98*rng.rand() - 0.5)));
    location_log_width = log(1E-3*image_length) + log(1E3)*rng.rand();
    scale_log_width = 3.0*rng.rand();
}

double MyConditionalPrior::perturb_hyperparameters(RNG& rng)
{
    double logH = 0.0;

    int which = rng.rand_int(3);

    if(which == 0)
    {
        mu_mass = log(mu_mass);
        mu_mass = (atan(mu_mass)/M_PI + 0.5 - 0.01)/0.98;
        mu_mass += rng.randh();
        wrap(mu_mass, 0.0, 1.0);
        mu_mass = tan(M_PI*(0.01 + 0.98*mu_mass - 0.5));
        mu_mass = exp(mu_mass);
    }
    else if(which == 1)
    {
        location_log_width += log(1E3)*rng.randh();
        wrap(location_log_width, log(1E-3*image_length), log(image_length));
    }
    else
    {
        scale_log_width += 3.0*rng.randh();
        wrap(scale_log_width, 0.0, 3.0);
    }

    return logH;
}

// vec[0] is mass
// vec[1] is log_width
double MyConditionalPrior::log_pdf(const std::vector<double>& vec) const
{
    double logp = 0.0;

    if(vec[0] < 0.0)
        return -std::numeric_limits<double>::max();

    logp += -log(mu_mass) - vec[0]/mu_mass;
    logp += laplacian_log_pdf(vec[1], location_log_width, scale_log_width);

    return logp;
}

// vec[0] is mass
// vec[1] is log_width
void MyConditionalPrior::from_uniform(std::vector<double>& vec) const
{
    vec[0] = -mu_mass*log(1.0 - vec[0]);
    vec[1] = laplacian_cdf_inverse(vec[1], location_log_width, scale_log_width);
}

// vec[0] is mass
// vec[1] is log width
void MyConditionalPrior::to_uniform(std::vector<double>& vec) const
{
    vec[0] = 1.0 - exp(-vec[0]/mu_mass);
    vec[1] = laplacian_cdf(vec[1], location_log_width, scale_log_width);
}

void MyConditionalPrior::print(std::ostream& out) const
{
    out<<mu_mass<<' ';
    out<<location_log_width<<' ';
    out<<scale_log_width<<' ';
}

// Laplacian cdf stuff
int MyConditionalPrior::sign(double x)
{
    if(x == 0.)
        return 0;
    if(x > 0.)
        return 1;
    return -1;
}

double MyConditionalPrior::laplacian_log_pdf
                        (double x, double center, double width)
{
    assert(width > 0.0);
    return -log(2*width) - std::abs(x - center)/width;
}


double MyConditionalPrior::laplacian_cdf(double x, double center, double width)
{
    assert(width > 0.0);
    return 0.5 + 0.5*sign(x - center)*(1. - exp(-std::abs(x - center)/width));
}

double MyConditionalPrior::laplacian_cdf_inverse(double x, double center,
                                                        double width)
{
    assert(width > 0.0);
    assert(x >= 0.0 && x <= 1.0);

    return center - width*sign(x - 0.5)*log(1. - 2.*std::abs(x - 0.5));
}


