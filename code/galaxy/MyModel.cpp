#include "MyModel.h"
#include "DNest4/code/DNest4.h"
#include "Data.h"
#include <iomanip>

using namespace std;
using namespace DNest4;

MyModel::MyModel()
:gaussians(2, 20, false, MyConditionalPrior(), PriorType::log_uniform)
,model_image(Data::get_instance().get_ni(),
                vector<double>(Data::get_instance().get_nj()))
{

}

void MyModel::from_prior(RNG& rng)
{
    // Grab stuff from the data
    double x_min = Data::get_instance().get_x_min();
    double x_max = Data::get_instance().get_x_max();
    double y_min = Data::get_instance().get_y_min();
    double y_max = Data::get_instance().get_y_max();

    gaussians.from_prior(rng);

    q = exp(rng.randn());
    xc = x_min + (x_max - x_min)*rng.rand();
    yc = y_min + (y_max - y_min)*rng.rand();
    theta = 2*M_PI*rng.rand();
    cos_theta = cos(theta);
    sin_theta = sin(theta);

    sigma0 = exp(tan(M_PI*(0.01 + 0.98*rng.rand() - 0.5)));
    sigma_coeff = exp(tan(M_PI*(0.01 + 0.98*rng.rand() - 0.5)));

    compute_model_image();
}

double MyModel::perturb(RNG& rng)
{
	double logH = 0.0;

    // Grab stuff from the data
    double x_min = Data::get_instance().get_x_min();
    double x_max = Data::get_instance().get_x_max();
    double y_min = Data::get_instance().get_y_min();
    double y_max = Data::get_instance().get_y_max();

    int which1 = rng.rand_int(3);
    int which2;

    if(which1 == 0)
    {
        logH += gaussians.perturb(rng);
        compute_model_image();
    }
    else if(which1 == 1)
    {
        which2 = rng.rand_int(4);
        if(which2 == 0)
        {
            q = log(q);
            logH -= -0.5*pow(q, 2);
            q += rng.randh();
            logH += -0.5*pow(q, 2);
            q = exp(q);
        }
        else if(which2 == 1)
        {
            xc += (x_max - x_min)*rng.randh();
            wrap(xc, x_min, x_max);
        }
        else if(which2 == 2)
        {
            yc += (y_max - y_min)*rng.randh();
            wrap(yc, y_min, y_max);
        }
        else
        {
            theta += 2*M_PI*rng.randh();
            wrap(theta, 0.0, 2*M_PI);
            cos_theta = cos(theta);
            sin_theta = sin(theta);
        }
        compute_model_image();
    }
    else
    {
        which2 = rng.rand_int(2);
        if(which2 == 0)
        {
            sigma0 = log(sigma0);
            sigma0 = (atan(sigma0)/M_PI + 0.5 - 0.01)/0.98;
            sigma0 += rng.randh();
            wrap(sigma0, 0.0, 1.0);
            sigma0 = tan(M_PI*(0.01 + 0.98*sigma0 - 0.5));
            sigma0 = exp(sigma0);
        }
        else
        {
            sigma_coeff = log(sigma_coeff);
            sigma_coeff = (atan(sigma_coeff)/M_PI + 0.5 - 0.01)/0.98;
            sigma_coeff += rng.randh();
            wrap(sigma_coeff, 0.0, 1.0);
            sigma_coeff = tan(M_PI*(0.01 + 0.98*sigma_coeff - 0.5));
            sigma_coeff = exp(sigma_coeff);
        }
    }

	return logH;
}

void MyModel::compute_model_image()
{
    // Get stuff from data
    const auto& x = Data::get_instance().get_x_rays();
    const auto& y = Data::get_instance().get_y_rays();

    // Transformed coordinates
    vector< vector<double> > xx(Data::get_instance().get_ni(),
                       vector<double>(Data::get_instance().get_nj()));
    vector< vector<double> > yy(Data::get_instance().get_ni(),
                       vector<double>(Data::get_instance().get_nj()));
    vector< vector<double> > rsq(Data::get_instance().get_ni(),
                       vector<double>(Data::get_instance().get_nj()));
    for(int i=0; i<Data::get_instance().get_ni(); ++i)
    {
        for(int j=0; j<Data::get_instance().get_nj(); ++j)
        {
            xx[i][j] =  cos_theta*(x[i][j] - xc) + sin_theta*(y[i][j] - yc);
            yy[i][j] = -sin_theta*(x[i][j] - xc) + cos_theta*(y[i][j] - yc);
            rsq[i][j] = q*pow(xx[i][j], 2) + pow(yy[i][j], 2)/q;
        }
    }

    // Zero the model image
    model_image.assign(Data::get_instance().get_ni(),
                       vector<double>(Data::get_instance().get_nj(), 0.0));

    // Add the gaussians
    const auto& components = gaussians.get_components();

    double mass, width, C1, C2;
    for(const auto& c: components)
    {
        mass = c[0];
        width = exp(c[1]);
        C1 = mass/(2.0*M_PI*width*width);
        C2 = 1.0/(width*width);

        for(size_t i=0; i<model_image.size(); ++i)
            for(size_t j=0; j<model_image.size(); ++j)
                model_image[i][j] += C1*exp(-0.5*C2*rsq[i][j]);
    }
}

double MyModel::log_likelihood() const
{
    const auto& images = Data::get_instance().get_images();
    const auto& image = images[0];
    const auto& sigmas = Data::get_instance().get_sigmas();
    const auto& sigma = sigmas[0];

	double logL = 0.0;

    double var;
    for(size_t i=0; i<model_image.size(); ++i)
    {
        for(size_t j=0; j<model_image.size(); ++j)
        {
            var = sigma[i][j] + sigma0*sigma0 + sigma_coeff*model_image[i][j];
            logL += -0.5*log(2*M_PI*var)
                    -0.5*pow(image[i][j] - model_image[i][j], 2)/var;
        }
    }

	return logL;
}

void MyModel::print(std::ostream& out) const
{
    out<<setprecision(6);
    for(size_t i=0; i<model_image.size(); ++i)
        for(size_t j=0; j<model_image.size(); ++j)
            out<<model_image[i][j]<<' ';

    out<<setprecision(10);
    gaussians.print(out);
    out<<q<<' '<<xc<<' '<<yc<<' '<<theta<<' ';
    out<<sigma0<<' '<<sigma_coeff<<' ';
}

string MyModel::description() const
{
	return string("gaussians, sigma0, sigma_coeff");
}

