#ifndef DNest4_Template_MyModel
#define DNest4_Template_MyModel

#include "DNest4/code/DNest4.h"
#include "MyConditionalPrior.h"
#include <ostream>

class MyModel
{
	private:
        // The gaussians
        DNest4::RJObject<MyConditionalPrior> gaussians;

        // Axis ratio, central position, orientation angle
        double q, xc, yc, theta, cos_theta, sin_theta;

        // Noise parameters;
        double sigma0, sigma_coeff;

        // Model image
        std::vector< std::vector<double> > model_image;
        void compute_model_image();

	public:
		// Constructor only gives size of params
		MyModel();

		// Generate the point from the prior
		void from_prior(DNest4::RNG& rng);

		// Metropolis-Hastings proposals
		double perturb(DNest4::RNG& rng);

		// Likelihood function
		double log_likelihood() const;

		// Print to stream
		void print(std::ostream& out) const;

		// Return string with column information
		std::string description() const;
};

#endif

