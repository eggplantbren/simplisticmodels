import numpy as np
import dnest4.deprecated as dn4
import matplotlib.pyplot as plt

# Set up Matplotlib stuff
plt.rc("font", size=20, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)
plt.rcParams["image.interpolation"] = "nearest"
plt.rcParams["image.cmap"] = "viridis"

# Load data
data = dn4.my_loadtxt("Data/test_image.txt")
metadata = np.loadtxt("Data/test_metadata.txt")
ni, nj = int(metadata[1]), int(metadata[2])
num_pixels = ni*nj
dx = (metadata[4] - metadata[3])/nj
dy = (metadata[6] - metadata[5])/ni
dA = dx*dy

# Load posterior samples
posterior_sample = dn4.my_loadtxt("posterior_sample.txt")

# Total flux
flux = np.empty(posterior_sample.shape[0])

for i in range(0, posterior_sample.shape[0]):
    image = posterior_sample[i, 0:num_pixels]\
                                  .reshape((ni, nj))
    flux[i] = image.sum()*dA

plt.figure(figsize=(13, 9))
plt.subplot(1, 2, 1)
plt.imshow(image, extent=metadata[3:7])
plt.title("Model Image")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.subplot(1, 2, 2)
plt.imshow(image - data, extent=metadata[3:7], cmap="coolwarm")
plt.title("Residuals")
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.show()

plt.hist(flux, 100, alpha=0.2)
plt.axvline(77.9015, color="r", linestyle="--")
plt.xlabel("Total Flux $M$")
plt.ylabel("Number of posterior samples")
np.savetxt("total_flux.txt", flux)
plt.show()

