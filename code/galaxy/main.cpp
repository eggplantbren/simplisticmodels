#include <iostream>
#include "DNest4/code/DNest4.h"
#include "MyModel.h"
#include "Data.h"

using namespace DNest4;

int main(int argc, char** argv)
{
    // Load the test data
    Data::get_instance().load("Data/test_metadata.txt", "Data/test_image.txt",
                              "Data/test_sigma.txt");

    // Run DNest4
	DNest4::start<MyModel>(argc, argv);

	return 0;
}

