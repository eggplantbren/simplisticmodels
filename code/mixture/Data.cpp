#include "Data.h"
#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

// The static instance
Data Data::instance;

Data::Data()
{

}

void Data::load(const char* filename)
{
	// Open the file
	fstream fin(filename, ios::in);
    if(!fin)
    {
        cerr<<"# Failed to open file "<<filename<<"."<<endl;
        return;
    }

    x.clear();

	// Temporary variables
	double temp;

	// Read until end of file
	while(fin>>temp)
        x.push_back(temp);

	// Close the file
	fin.close();
}

