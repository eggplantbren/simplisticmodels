#ifndef SimplisticModels_Data
#define SimplisticModels_Data

#include <vector>

/*
* An object of this class is a dataset
*/
class Data
{
	private:
		// The data points
		std::vector<double> x;

	public:
		// Constructor
		Data();

		// Load data from a file
		void load(const char* filename);

		// Access to the data points
		const std::vector<double>& get_x() const
		{ return x; }

	private:
		// Static "global" instance
		static Data instance;

	public:
		// Getter for the global instance
		static Data& get_instance()
		{ return instance; }
};

#endif

