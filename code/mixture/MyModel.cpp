#include "MyModel.h"
#include "Data.h"
#include "DNest4/code/DNest4.h"

using namespace std;
using namespace DNest4;

const Cauchy MyModel::cauchy(0.0, 5.0);

MyModel::MyModel()
{

}

void MyModel::from_prior(RNG& rng)
{
    mu = cauchy.generate(rng);
    log_sigma = cauchy.generate(rng);
}

double MyModel::perturb(RNG& rng)
{
	double logH = 0.0;

    int which = rng.rand_int(2);

    if(which == 0)
    {
        logH += cauchy.perturb(mu, rng);
    }
    else
    {
        logH += cauchy.perturb(log_sigma, rng);
    }

	return logH;
}

double MyModel::log_likelihood() const
{
    // Get the data
    const auto& x = Data::get_instance().get_x();

    double sigma = exp(log_sigma);
    double logL = 0.0;

    for(double xx: x)
    {
        logL += -log(M_PI*sigma) - log(1.0 + pow((xx - mu)/sigma, 2));
    }
    if(std::isnan(logL) || std::isinf(logL))
        logL = -1E300;

	return logL;
}

void MyModel::print(std::ostream& out) const
{
    out<<mu<<' '<<exp(log_sigma)<<' ';
}

string MyModel::description() const
{
	return string("");
}

