import numpy as np
import numpy.random as rng
import matplotlib.pyplot as plt
import dnest4.classic as dn4

def cauchy(x, params):
    return 1.0/np.pi/params[1]/(1.0 + ((x - params[0])/params[1])**2)

data = np.loadtxt("data.txt")
posterior_sample = dn4.my_loadtxt("posterior_sample.txt")

x = np.linspace(-5.0, 5.0, 10001)

plt.hist(data, 100, color="k", alpha=0.2, normed=True)
plt.hold(True)
for i in range(0, posterior_sample.shape[0]):
    plt.plot(x, cauchy(x, posterior_sample[0, :]), "g", alpha=0.2)
plt.xlim([x.min(), x.max()])
plt.show()

