import numpy as np
import numpy.random as rng
import matplotlib.pyplot as plt

# Set the rng seed
rng.seed(0)

# Size of dataset
N = 10000

# A 50/50 mixture of two gaussians
x = np.hstack([rng.randn(N//2), 1.0 + 0.3*rng.randn(N//2)])

# Save the data
np.savetxt("data.txt", x)

# Plot the data
plt.hist(x, 100, color="k", alpha=0.2)
plt.show()

# Cauchy fitted to true density
x = np.linspace(-100.0, 100.0, 100001)
f = 0.5*np.exp(-0.5*x**2)/np.sqrt(2*np.pi)\
        + 0.5*np.exp(-0.5*(x - 1.0)**2/0.3**2)/np.sqrt(2*np.pi*0.3**2)
f /= np.trapz(f, x=x)

def cauchy(x, params):
    w = np.exp(params[1])
    p = 1.0/np.pi/w/(1.0 + ((x - params[0])/w)**2)
    p /= np.trapz(p, x=x)
    return p

def badness(params):
    y = cauchy(x, params)
    return np.trapz(f*np.log(f/y + 1E-300), x=x)

import scipy.optimize
params = scipy.optimize.minimize(badness, [0.0, 0.0]).x
print(params[0], np.exp(params[1]))

plt.plot(x, f, "k")
plt.hold(True)
plt.plot(x, cauchy(x, params), "g")
plt.show()

