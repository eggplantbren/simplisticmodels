import numpy as np
import matplotlib.pyplot as plt

import plot_utils

def stretch(x):
    y = x - x.min()
    y /= y.max()
    return y**0.25

# Load the data
image = np.loadtxt("../code/galaxy/Data/test_image.txt")

plt.imshow(stretch(image), extent=[-1.0, 1.0, -1.0, 1.0])
plt.xlabel("$x$")
plt.ylabel("$y$")
plt.title("A Complex Galaxy")
plt.savefig("figures/galaxy_data.pdf", bbox_inches="tight")

