import plot_utils
import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0.0, 10.0, 256)
y = np.linspace(0.0, 10.0, 256)
h = x[1] - x[0]

x, y = np.meshgrid(x, y)
y = y[::-1, :]

f = np.exp(-0.5*(y - (x/10.0)**0.7*10.0)**2/2**2)*(1.0 - x/10.0)
f /= h**2*f.sum()

plt.imshow(-f, cmap="gray", interpolation="nearest",\
                extent=[0.0, 10.0, 0.0, 10.0])
plt.xlabel("$L_1$")
plt.ylabel("$L_2$")
plt.title("$\\pi(L_1, L_2)$")
plt.savefig("figures/l1l2.pdf", bbox_inches="tight")


