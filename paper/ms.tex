\documentclass[a4paper, 12pt]{article}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[left=2cm, right=2cm, bottom=2.5cm, top=2cm]{geometry}
\usepackage{microtype}
\usepackage{natbib}

% Some aliases for boldsymbols
\newcommand{\btheta}{\boldsymbol{\theta}}
\newcommand{\bx}{\boldsymbol{x}}

\title{On simplistic modelling assumptions in probabilistic inference}
\author{Brendon J. Brewer}
\date{}

\begin{document}
\maketitle

\abstract{\noindent To do practical inference, scientists often use
simplifying assumptions that cannot be strictly true.
Yet, we want to obtain reliable and trustworthy results.
Sometimes, the results would not change if more realistic assumptions
had been incorporated, in which case, nothing needs to be done.
However, sometimes the simplistic assumptions are fatal to the analysis,
giving absurd estimates or unrealistically tiny uncertainties.
Simplistic models have two big advantages: i) they're easy to use; and
ii) the results are easy to interpret. In this paper, I describe and
attempt to justify methods that can be used to widen the range of
applicability of simplistic models.}

% Need this after the abstract
\setlength{\parindent}{0pt}
\setlength{\parskip}{8pt}

\section{Introduction}

Probability theory quantifies the degree to which propositions imply each other
\citep{knuth2012foundations}.
Specifically,
the conditional probability $P(A | B, C)$ is the degree to which $B$ implies
$A$, within the context $C$. The two rules of probability theory are the
product rule
\begin{align}
P(A, B | C) &= P(A | C)P(B | A, C)
\end{align}
and the sum rule
\begin{align}
P(A \vee B | C) &= P(A | C) + P(B | C) - P(A, B | C)
\end{align}
where comma denotes logical {\em and} and $\vee$ denotes logical {\em or}.
When applied to the question of how strongly a dataset implies a particular
conclusion, this use of probability theory is known as {\em Bayesian inference}.

Traditionally, the propositions of interest are organised in
``parameter estimation'' terms.
Within the context of prior information $I$,
the posterior distribution for unknown quantities $\theta$
given data $D$ (which describes partial knowledge about $\theta$
and takes the data into account) is:
\begin{align}
p(\theta | D, I) &= \frac{p(\theta | I)p(D | \theta, I)}
                               {p(D | I)}
\end{align}
The parameter space is a set of mutually exclusive and exhaustive (given $I$)
propositions about what the value of $\theta$ might be.

Ideally, $I$ (which stands for {\em information})
should represent all the prior information we actually have.
However, in practice, specifying $I$ explicitly is often difficult.
To avoid it, we tend to just assume something for
convenience. Let $A$ (for {\em assumption}) be the
convenient simplifications we use. The posterior we actually obtain
using the simplistic assumptions $A$
is then:
\begin{align}
p(\theta_A | D, A) &= \frac{p(\theta_A | A)p(D | \theta_A, A)}
                               {p(D | A)}
\end{align}
where $\theta_A$ are the unknown parameters when assumption $A$ is used.

In general, the unknown parameters $\theta_A$ given the simplistic
assumptions are not the same as $\theta$, which we'd use in the ideal case.
The parameters $\theta_A$
may not even be well-defined unless $A$ is assumed.
For example, when fitting a straight line $y=mx + b$ to some noisy data
\citep{hoggFitLine},
$\theta_A = \{m, b\}$,
while $A$ implies that the relationship between the two variables
is a straight line in the first place. Our ``real'' prior information, $I$,
might include the possibility that the relationship between the two variables
is curved, in which case the $\theta_A$ parameters no longer
have a precise meaning.
Despite this, it may be possible to ``rescue'' the
meaning of the simple model parameters even when we do not assume $A$.
In the regression case, a non-straight
curve $f(x)$ still has a $y$-intercept, $f(0)$. If $f$ is
differentiable there are also many slopes, $f'(x)$. This
can be used to define average slopes over intervals, i.e.,
$1/(b-a) \times \int_a^b f'(x) \, dx$. These are
{\em generalisations} of $m$ and $b$ that apply even when
$A$ is not conditioned upon.

We often use posterior distributions [either $p(\theta|D,I)$, if we had it,
or $p(\theta_A | D, A)$]
to calculate our posterior state of knowledge about some other quantity $x$
which isn't a model parameter, but is predictable from them. This can be done
either under $I$ or $A$, giving two different posterior distributions for $x$:
\begin{align}
p(x | D, I) &=
\int p(x | \theta, D, I)p(\theta | D, I)\, d\theta\\
p(x | D, A) &=
\int p(x | \theta_A, D, A)p(\theta_A | D, A)\, d\theta_A.
\end{align}

Often, inferences under $I$ are impractical. Perhaps it would be too
computationally expensive, or it would take too long to assign sensible priors.
We might also want to take advantage of easily-interpreted simplistic
model parameters $\theta_A$. In these situations, we can go ahead and use
the simplistic assumptions $A$.

However, this can lead to overconfident results when the simplistic model
is used to compute posterior distributions such as $p(x | D, A)$.
Section~\ref{sec:good_enough} discusses this issue, and discusses
methods to correct for it without the need to implement the realistic
assumptions $I$.

\section{Density estimation example}
Consider a standard statistical ``sampling'' situation, where
a frequency distribution $f(x)$ exists, and we want to learn about it
by obtaining samples
$\bx = \{x_1, x_2, ..., x_N\}$. Suppose we can parameterize
the set of possibilities for $f(x)$ using parameters $\btheta$,
and make the standard
assumption about the prior information, namely, that the
conditional prior for the data given the function $f$ is
\begin{align}
p(\bx | \btheta) &= \prod_{i=1}^N f(x_i; \btheta).
\end{align}
In standard parlance, the samples are independent and identically
distributed, conditional on $\btheta$.

For familiarity, let the simple assumption $A$ be that $f(x)$ is a gaussian,
such that the simple model parameters are the mean and standard deviation
of the gaussian, $\btheta_A = \{\mu, \sigma\}$. It is straightforward to
obtain the posterior (here, assuming a standard `uninformative' prior):
\begin{align}
p(\mu, \sigma | \bx, A)
    &\propto p(\mu, \sigma | A)p(\bx, \mu, \sigma, A)\\
    &= p(\mu, \sigma)\prod_{i=1}^N \frac{1}{\sigma\sqrt{2\pi}}
            \exp\left[-\frac{1}{2\sigma^2}\left(x_i - \mu\right)\right]\\
    &= \frac{1}{\sigma}(\sigma\sqrt{2\pi})^{-N}
        \exp\left[-\frac{1}{2\sigma^2}\sum_{i=1}^N (x_i - \mu)^2\right]\\
    &\propto \sigma^{-N-1}
        \exp\left[-\frac{1}{2\sigma^2}\sum_{i=1}^N (x_i - \mu)^2\right].
\end{align}

This can be factored into a Gamma(shape=, scale=) distribution for
$1/\sigma^2$, and a Normal($\bar{x}$, ) distribution for
$\mu$ given $\sigma$.

\subsection{MaxEnt}
The joint prior can be derived from maximum entropy with a flat pre-prior
over the $(\mu, \sigma, \bx)$, and the following constraints:
\begin{enumerate}
\item The marginal distribution for $(\mu, \sigma)$ ought to be
proportional to $1/\sigma$.\\
\item The conditional distribution $p(\bx | \mu, \sigma)$ ought to
imply that $\left<\bx\right> = \mu$ and ...
\end{enumerate}

\section{Galaxy example}
To keep things concrete, I will use a semi-realistic example throughout the
paper. Fitting surface brightness profiles (essentially 2D ``densities'')
to noisy images of galaxies is a common task in astronomy.

Figure~\ref{fig:galaxy_data} shows a simulated noisy image of a galaxy.
This is the dataset $D$ for this example.
The true surface brightness profile was a mixture of 10
two-dimensional elliptical gaussians:
\begin{align}
m(x, y) &= \sum_{i=1}^{10} \frac{1}{2\pi}
            \frac{M_i}{w_i^2}
            \exp\left[-\frac{1}{2w_i^2}(qx'^2 + y'^2/q)\right]
\end{align}
where $M_i$ is the overall flux of gaussian $i$,
$w_i$ is its width, and $q$ is the (common)
axis ratio. The transformed coordinates
$x'$ and $y'$ result from rotating by an angle $\theta$.
Noise was added from a normal distribution with mean zero and standard deviation
$\sigma$.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.5]{figures/galaxy_data.pdf}
\caption{\it A simulated noisy dataset (image) of a galaxy.
\label{fig:galaxy_data}}
\end{figure}

The total flux of the galaxy is the integral of $m(x, y)$ over the entire
domain. Suppose we want to know the total flux within the image
\begin{align}
M &= \int\limits_{-1}^1 \int\limits_{-1}^1 m(x, y) \, dx \, dy.
\end{align}
The true value of $M$ was 77.9015, to six significant figures.

A simple unbiased (in the frequentist sense) estimator of the total flux
is just the sum of all the pixels in the image:
\begin{align}
\hat{M} &= \sum_i D_i.
\end{align}

If we use an accurate, complex model to fit the image, we get the following
result:

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{figures/total_flux_posteriors.pdf}
\caption{\em Posterior distributions for the total flux $M$ of the galaxy
given the noisy data, under the simplistic assumptions, and under the
``realistic'' assumptions. As you might expect, the realistic model performed
best (i.e. the posterior density at the true value is much higher).
\label{fig:total_flux_posteriors}}
\end{figure}




\section{Simplistic models can be ``good enough'', or not}\label{sec:good_enough}
It still might be worthwhile to calculate the posterior
$p(\theta_A | D, A)$. After all, it tells us how strongly
$D$ {\bf and} $A$ imply certain values of $\theta_A$. That's potentially
worth knowing, even if we don't believe $A$. The parameters $\theta_A$ still
tell us something.

We can also use the posterior $p(\theta_A | D)$
to calculate $p(x | D, A)$.
If $A$ is a ``good enough'' model, $p(x | D, A)$
might be a very good approximation to $p(x | D, I)$
for quantities $x$ that make sense under both $A$ and $I$.
In this case, we can simply adopt $p(x | D, A)$
as ``our posterior'' for $x$ even though in principle we should have
calculated $p(x | D, I)$.

The marginal likelihood of $A$,
$p(D | A) = \int p(\theta_A | A)p(D | \theta_A, A)\, d\theta_A$,
can also be used to calculate the posterior probability
of $A$ if explicit alternatives are introduced.

\section{Simplistic models can be {\em terrible}}
Sometimes, using $A$ when we should have used $I$ leads to disaster.
%Sticking with the Sérsic example, the uncertainties you get when you fit
%such a model to a big image (1000 $\times$ 1000 pixels, say), are ridiculously
%small. Say you were interested in the galaxy's total flux:
%\begin{align}
%M &= \iint m(x, y) \, dx \, dy
%\end{align}
%The posterior distribution $p(M | D, A)$ will probably be far narrower
%than $p(M | D, I)$.

\section{Theory}
When using our simplistic posterior $p(\theta_A | D, A)$ to infer
some quantity $x$, we want $p(x | D, A)$ to be a good approximation to
$p(x | D, I)$. If we want to be specific about how good the approximation
is, we can quantify the closeness of the approximation using
the Kullback-Leibler divergence (either from $p(x | D, I)$ to
$p(x | D, A)$, or the other way around).

Consider the ``full hypothesis space'' $\Theta$ implied by $I$.
The model parameters $\theta$ give us a coordinate system on $\Theta$.
The posterior $p(\theta | D, I)$ is a probability distribution over
$\Theta$.
The simplistic assumptions $A$ imply a different hypothesis space $\Theta_A$,
and $\theta_A$ provides a coordinate system on $\Theta_A$.

One advantage of simplistic assumptions is that $\theta_A$ often has a
nice, clear interpretation. Can we rescue this advantage?
Yes. We can do this by defining a mapping from $\Theta$ to $\Theta_A$.
For any possible ``complex truth'' $\theta \in \Theta$, we can define a
corresponding
simplistic model $\theta_A \in \Theta_A$. We might do this by insisting
that the simplistic model parameters $\theta_A$ are those that
make the simplistic model {\em as close as possible} to the truth $\theta$
in a sense that we get to define.
See Figure~\ref{fig:parameter_space} for an illustration of this idea.
The cube represents a full, realistic hypothesis space $\Theta$,
indexed by coordinates $\theta$.
The hypothesis space of the simple approximation, $\Theta_A$, is represented
here as the blue surface, and is indexed by $\theta_A$.
It is depicted as a subset of $\Theta$, although this
needn't be the case in general.
We can ``rescue the meaning'' of the simple model parameters even when the
true situation is not in $\Theta_A$. Suppose the red circle is the true
situation, and the green star (which is in $\Theta_A$) is the closest
approximation. Then the value of $\theta$ corresponding to the green star
may also be assigned to the red circle.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.6]{figures/parameter_space.pdf}
\caption{\it \label{fig:parameter_space}}
\end{figure}

Now let's return to the problem of inferring $x$.
If we have done computation on the simplistic model,
we have access to more distributions than
just $p(x|D, A)$. So we may not need to give up if this is a
bad approximation to $p(x|D,I)$.
In the course of computing $p(x|D, A)$, especially if we used Nested
Sampling (or similar), we can access other probability distributions as well,
some of which might be close to $p(x | D, I)$.
Why not try them? Here is a family of
distributions (for $\theta_A$, not $x$)
we can get from a Nested Sampling run --- the canonical family:
\begin{align}
p(\theta_A; \lambda) &= \frac{\pi(\theta_A)L(\theta_A)^\lambda}{Z(\lambda)}.
\end{align}

For any value of $\lambda$, we can use $p(\theta_A; \lambda)$ to get a
distribution over $x$:
\begin{align}
p(x; \lambda) &=
\int p(x | \theta_A, D, A)
\frac{\pi(\theta_A)L(\theta_A)^\lambda}{Z(\lambda)} \, d\theta_A
\end{align}
How good an approximation is this to $p(x|D, I)$? Let's use the KL
divergence again. I think the right KL divergence
for approximations is the one from the approximation (as the source) to
the true distribution (as the destination).
However, this is typically intractable since it involves
an expectation with respect to the very distribution we're trying to
approximate. So here I'll use the standard KL divergence as used in
``variational Bayes'', with the approximation as the destination and
the true distribution as the source. That is:
\begin{align}
D_{\rm KL}\left\{p(x; \lambda); p(x|D, I)\right\}
&= \int_x p(x; \lambda) \log \frac{p(x; \lambda)}{p(x | D, I)} \, dx
\end{align}

Using $p(x | D, I) = p(x, D | I)/p(D, I)$, we have
\begin{align}
D_{\rm KL}\left\{p(x; \lambda); p(x|D, I)\right\}
&= \int_x p(x; \lambda) \log \frac{p(x; \lambda)}{p(x, D | I)} \, dx
+\log p(D|I)
\end{align}
Making the log evidence the subject gives
\begin{align}
\log p(D|I)
&= D_{\rm KL}\left\{p(x; \lambda); p(x|D, I)\right\}
- \int_x p(x; \lambda) \log \frac{p(x; \lambda)}{p(x, D | I)} \, dx
\end{align}
When varying $\lambda$, this remains constant. So minimising the KL divergence
is equivalent to minimising the integral
\begin{align}
f(\lambda)
&= \int_x p(x; \lambda) \log \frac{p(x; \lambda)}{p(x | I)p(D | x, I)} \, dx\\[2ex]
&= \int_x p(x; \lambda) \log \frac{p(x; \lambda)}{p(x | I)} \, dx
    - \int_x p(x; \lambda) \log p(D | x, I) \, dx.
\end{align}
It's unfortunate that this depends on $p(D | x, I)$. This means we can't
actually minimise this integral without explicitly specifying $I$ and
doing calculations with it --- which is something I'd like to avoid.

As a special case, we can set $x = \theta_A$ and also suppose
$p(\theta_A | I) = p(\theta_A | A)$. Then, $f$ becomes
\begin{align}
f(\lambda)
&= \int_{\theta_A} p(\theta_A; \lambda) \log \frac{p(\theta_A; \lambda)}{p(\theta_A | A)} \, d\theta_A
- \int_{\theta_A} p(\theta_A | A) \log p(D | \theta_A, I) \, d\theta_A.
\end{align}
The first term is the KL divergence of the approximate posterior from the
prior, and the second term is an expected value with respect to the
approximate posterior.
Now we're getting closer to something we can use in practice, since
$p(\theta_A | A)$ is fairly easy to assign. The remaining difficult term
is $p(D | \theta_A, I)$.

Let $\theta = \{\theta_A, \xi\}$, i.e. let $\xi$ be the extra coordinates
needed to specify a location in $\Theta$. Then
\begin{align}
p(D|\theta_A, I) &= \int p(\xi, D | \theta_A, I) \, d\xi\\[2ex]
&= \int p(\xi | \theta_A, I)p(D | \xi, \theta_A, I) \, d\xi
\end{align}

\section{Nested sampling implementation}
Run standard NS with the simple model's likelihood, but understand the
choice of approximation by thinking about the TwinPeaks framework
with the simplistic likelihood and the realistic likelihood, which ought
to be strongly correlated.

\begin{figure}[!ht]
\centering
\includegraphics[scale=0.6]{figures/l1l2.pdf}
\caption{\label{fig:l1l2}}
\end{figure}

\section{Galaxy Example}

For the specific case of images, we can use the divergence of measures:
\begin{align}
\textnormal{Divergence}\left(\theta; \theta_A\right)
&= \iint \left[m_A(x,y) - m(x,y) + m(x,y)\log\frac{m(x,y)}{m_A(x,y)}\right]
\, dx \, dy
\end{align}
and find the simplistic parameters via
\begin{align}
\theta_A &= \textnormal{arg}\min\left\{
\textnormal{Divergence}\left(\theta; \theta_A\right)\right\}
\end{align}

\bibliographystyle{chicago}
\bibliography{references}

\end{document}

