import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import plot_utils

def function(x, y):
    return 0.8 - (x - 0.5)**2 - (y - 0.5)**2 + x*y

# Set up 3D axes
fig = plt.figure(figsize=(9, 8))
ax = fig.add_subplot(111, projection="3d")

# Coordinates
x = np.linspace(0.0, 1.0, 51)
y = np.linspace(0.0, 1.0, 51)
[x, y] = np.meshgrid(x, y)

plt.hold(True)

# Wireframe plot
f = function(x, y)
ax.plot_surface(x, y, f, rstride=1, cstride=1, cmap="Blues",\
                            linewidth=0.3, alpha=0.1)

# A point off the surface and a point on it
x1, y1 = 0.3, 0.2
ax.scatter(x1, y1, 1.0, s=50, c="r")

x2, y2 = 0.4, 0.11
ax.scatter(x2, y2, function(x2, y2), s=150, c="g", marker="*")
ax.plot([x1, x2], [y1, y2], [1.0, function(x2, y2)], color="k", linewidth=1,\
                                                        linestyle="--")

# Axis labels etc
ax.set_xlim([0.0, 1.0])
ax.set_ylim([0.0, 1.0])
ax.set_zlim([f.min(), f.max()])
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])
ax.view_init(20.0, 340.0)

plt.savefig("figures/parameter_space.pdf", bbox_inches="tight")

