import matplotlib.pyplot as plt

# Set up Matplotlib stuff
plt.rc("font", size=20, family="serif", serif="Computer Sans")
plt.rc("text", usetex=True)
plt.rcParams["image.interpolation"] = "nearest"
plt.rcParams["image.cmap"] = "viridis"

