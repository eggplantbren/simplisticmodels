import numpy as np
import matplotlib.pyplot as plt
import plot_utils

simplistic_model_flux = np.loadtxt("../code/galaxy/Runs/Simplistic/total_flux.txt")
full_model_flux = np.loadtxt("../code/galaxy/Runs/Realistic/total_flux.txt")

bins = np.linspace(5.0, 80.0, 301)
plt.hist(simplistic_model_flux, bins=bins, normed=True, color="blue", alpha=0.2,\
                label="Simplistic Model")
plt.hist(full_model_flux, bins=bins, normed=True, color="green", alpha=0.2,\
                label="Realistic Model")
plt.axvline(77.9015, color="k", linestyle="--", label="Truth")
plt.axvline(78.0121, color="r", linestyle="-.", label="Naive image sum")
plt.xlabel("Total Flux $M$")
plt.ylabel("Posterior density")
plt.axis([56, 80, 0, 3])
plt.legend(loc="upper left", fontsize=16)
plt.savefig("figures/total_flux_posteriors.pdf", bbox_inches="tight")

